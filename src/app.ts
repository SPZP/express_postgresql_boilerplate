import demoLogger from "./middlewares/log.middleware";
require("dotenv").config();
import express from "express";
import userRoutes from "./user/routes";
import bodyParser from "body-parser";
import fs from "fs";
import { CronJob } from "cron";
import cors from "cors";
import orderRoutes from "./order/routes";

const app = express();

app.use(express.static("static"));

const dayStart = new Date().setUTCHours(0, 0, 0, 0);
let logFile = fs.createWriteStream(__dirname + "/logs/" + `${dayStart}.log`);

app.use(demoLogger);

// parse requests of content-type - application/json
app.use(bodyParser.json());
app.use(bodyParser.text());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use((...args) => demoLogger(logFile, ...args));

app.use(cors());
app.use("/users", userRoutes);
app.use("/", orderRoutes);

// define a route handler for the default home page
app.get("/", (req, res) => {
  res.send("Hello world!");
});

const job = new CronJob("00 00 00 * * *", () => {
  logFile = fs.createWriteStream(__dirname + "/logs/" + `${Date.now()}.log`);
});

job.start();

app.listen(process.env.SERVER_PORT, () => {
  console.log(
    `Server has been started at http://localhost:${process.env.SERVER_PORT}`
  );
});

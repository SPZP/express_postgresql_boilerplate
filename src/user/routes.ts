import { Router } from "express";
import {handleUsersGet, handleUserGet, handleUserAvatar} from "./controllers";
import { registerUser, loginUser } from "../middlewares/auth.middleware";
import multer from "multer"
import multerS3 from "multer-s3"
import aws from "aws-sdk"

aws.config.update({
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
});

const s3 = new aws.S3();
const userRoutes = Router();

const imageFilter = (req: any, file: any, cb: any) => {
    if (!file.originalname.match(/\.(JPG|jpg|jpeg|png|gif)$/)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

export const upload = multer({
    fileFilter: imageFilter,
    storage: multerS3({
        acl: 'public-read',
        s3,
        bucket: `${process.env.AWS_BUCKET_NAME}`,
        cacheControl: 'max-age=31536000',
        metadata: function (req:any, file:any, cb:any) {
            cb(null, { fieldName: file.fieldname });
        },
        key: function (req:any, file:any, cb:any) {
            cb(null, `${Date.now().toString()}-${file.originalname}`)
        }
    })
})

userRoutes.get("/", handleUsersGet);
userRoutes.get("/:id", handleUserGet);
userRoutes.post("/register", registerUser);
userRoutes.post("/login", loginUser);
userRoutes.post('/:id/upload', upload.single('file'), handleUserAvatar);

export default userRoutes;

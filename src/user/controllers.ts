import {getUsers, getUserById, updateUser} from "./services";
import express from "express";

export async function handleUsersGet(
  req: express.Request,
  res: express.Response
) {
  try {
    const allUsers = await getUsers();
    res.json(allUsers);
    res.status(200);
  } catch (error) {
    res.status(404).end();
  }
}

export async function handleUserGet(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const userID = req.params.id;
    const someUser = await getUserById(userID);
    res.json(someUser);
    res.status(200);
  } catch (error) {
    res.status(404).end();
  }
  next();
}

export async function handleUserAvatar(req: any, res: express.Response) {
  try {
    let user = await getUserById(req.params.id);
    const updAvatar = await updateUser({...user, avatar:req.file.location});
    res.send(updAvatar);
    console.log("File was successfully uploaded!")
    res.end();
  } catch (error) {
    console.error("handleUserAvatar is wrong!");
  }
}

# Authorization with JWT token
## Endpoints

- Register new user
- METHOD: POST
- BODY: (first_name: string, last_name:string, email: string, customer_id:string, password:string, avatar:string)

   ```
   http://localhost:3000/users/register
   ```

- Login user
- METHOD: POST
- BODY: (email: string, password:string)

  ```
   http://localhost:3000/users/login
   ```

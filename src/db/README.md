# Sequelize settings. 

## Set up
```
npx sequelize-cli init
```

### This will create following folders
```
config, contains config file, which tells CLI how to connect with database
models, contains all models for your project
migrations, contains all migration files
seeders, contains all seed files
```

### .sequelizerc file (used by sequelize-cli command)

```
config - specify folder and extension of file with database connection configuration
models-path - file path of models
seeders-path - file path of seeders
migrations-path - file path of migrations
```


### config/connection_config.ts

```
first key - is environment for which config specified

username - name of user to connect to db
password - password of user
database - db name in DBMS
host - host of DBMS
dialect - type of database
```
### Creating the first Model

```
npx sequelize-cli model:generate --name users --attributes 
first_name:string,
last_name:string,
email:string,
customer_id:string,
password:string,
avatar:string,
```
### This will:

```
Create a model file user in models folder;
Create a migration file with name like XXXXXXXXXXXXXX-create-users.js in migrations folder.
```
### Running migration
```
npx sequelize-cli db:migrate
```

## Endpoints
- Get all users
- METHOD: GET

   ```
   http://localhost:3000/users
   ```

- Get some users
- METHOD: GET

   ```
   http://localhost:3000/users/:id
   ```

- Register new user
- METHOD: POST
- BODY: (first_name: string, last_name:string, email: string, customer_id:string, password:string, avatar:string)

   ```
   http://localhost:3000/users/register
   ```

- Login user
- METHOD: POST
- BODY: (email: string, password:string)

  ```
   http://localhost:3000/users/login
   ```

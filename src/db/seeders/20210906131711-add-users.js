'use strict';
const bcrypt = require("bcrypt")

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('users',[
        {
          first_name: 'Lionel',
          last_name: 'Messi',
          email: 'LMessi@gmail.com',
          customer_id:'cus_KG4Vg5ZBqCxmtu',
          password: await bcrypt.hash('password', 10),
          created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
          updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
        },
        {
          first_name: 'Cristiano',
          last_name: 'Ronaldo',
          email: 'CRonaldo@gmail.com',
          customer_id:'cus_KG4V4MsCN5xNHo',
          password: await bcrypt.hash('password', 10),
          created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
          updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
        },
        {
          first_name: 'Kilian',
          last_name: 'Mbappe',
          email: 'KMbappe@gmail.com',
          customer_id:'cus_KG4V90fLhy6yfN',
          password: await bcrypt.hash('password', 10),
          created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
          updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
        },
      {
        first_name: 'Sergio',
        last_name: 'Ramos',
        email: 'SRamos@gmail.com',
        customer_id: null,
        password: await bcrypt.hash('password', 10),
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    ])
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', null, {});
  }
};

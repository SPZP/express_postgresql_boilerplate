'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require("bcrypt")

module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    static associate(models) {
      users.hasOne(models.transactions, {
        foreignKey: "user_id"
      })
      users.hasMany(models.cards, {
        foreignKey: "user_id"
      })
      // define association here
    }
  }

  users.init({
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    email: DataTypes.STRING,
    customer_id: DataTypes.STRING,
    password: DataTypes.STRING,
    avatar: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'users',
    underscored: true,
  });
  users.addHook('beforeCreate', async (users) => {
    users.password = await bcrypt.hash(users.password, 10)
  })
  return users;
};
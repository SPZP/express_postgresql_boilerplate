'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class transactions extends Model {
    static associate(models) {
      // define association here
      transactions.belongsTo(models.users)
      transactions.belongsTo(models.products)
    }
  };
  transactions.init({
    token_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    amount: DataTypes.INTEGER,
    product_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'transactions',
    underscored: true,
  });
  return transactions;
};
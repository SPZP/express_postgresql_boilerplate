#Stripe payment system 

## Create a customer

```
const stripe = require('stripe')('secret_key');
const customer = await stripe.customers.create({
description: 'My First Test Customer (created for API docs)',
});

Parameters for example:

{
  "id": "cus_KHVtLhAwDpVeqx",
  "object": "customer",
  "address": null,
  "balance": 0,
  "created": 1632422483,
  "currency": "usd",
  "default_source": null,
  "delinquent": false,
  "description": "My First Test Customer (created for API docs)",
  "discount": null,
  "email": null,
  "invoice_prefix": "09AF2BA",
  "invoice_settings": {
    "custom_fields": null,
    "default_payment_method": null,
    "footer": null
  },
  "livemode": false,
  "metadata": {},
  "name": null,
  "next_invoice_sequence": 1,
  "phone": null,
  "preferred_locales": [],
  "shipping": null,
  "tax_exempt": "none"
}
```

## Create a card

```
const stripe = require('stripe')('secret_key');
const card = await stripe.customers.createSource(
  'cus_KHVtHlNoQx8eq1',
  {source: 'tok_mastercard'}
);
});

Parameters for example:

{
  "id": "card_1JcwrKDMvxQ5xDjv34pvTGGj",
  "object": "card",
  "address_city": null,
  "address_country": null,
  "address_line1": null,
  "address_line1_check": null,
  "address_line2": null,
  "address_state": null,
  "address_zip": null,
  "address_zip_check": null,
  "brand": "Visa",
  "country": "US",
  "customer": "cus_KHVtHlNoQx8eq1",
  "cvc_check": "pass",
  "dynamic_last4": null,
  "exp_month": 8,
  "exp_year": 2022,
  "fingerprint": "pZoHMKlDWtyvXLS4",
  "funding": "credit",
  "last4": "4242",
  "metadata": {},
  "name": null,
  "tokenization_method": null
}
```

### Other required information: https://stripe.com/docs/api
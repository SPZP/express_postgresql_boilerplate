import { Router } from "express";
import { handlePayment } from "./controllers";

const orderRoutes = Router();

orderRoutes.post("/payment", handlePayment);

export default orderRoutes;
